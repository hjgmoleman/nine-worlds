
# nine-worlds

nine-worlds

## Index

### Variables

* [mainWindow](README.md#markdown-header-let-mainwindow)

### Functions

* [createWindow](README.md#markdown-header-createwindow)
* [main](README.md#markdown-header-main)

## Variables

### `Let` mainWindow

• **mainWindow**: *BrowserWindow*

Defined in main.ts:4

## Functions

###  createWindow

▸ **createWindow**(): *void*

Defined in main.ts:6

**Returns:** *void*

___

###  main

▸ **main**(): *void*

Defined in renderer.ts:7

**Returns:** *void*
