import { ComponentConstructor, ComponentInitialisator } from './component';

function bittest(num: number, bit: number): boolean {
    return ((num >> bit) % 2 != 0)
}

function bitset(num: number, bit: number): number {
    return num | 1 << bit;
}

// function bitclear(num: number, bit: number) {
//     return num & ~(1 << bit);
// }

// function bittoggle(num: number, bit: number) {
//     return bittest(num, bit) 
//         ? bitclear(num, bit) 
//         : bitset(num, bit);
// }

export type ComponentsHash = number;

export const componentHashHasComponent = (hash: ComponentsHash, component: ComponentConstructor): boolean => {
    return bittest(hash, component.id);
}

export const getComponentsHashFromInitialisators = (components: ComponentInitialisator[]): ComponentsHash => {
    let num = 0;
    for (const comp of components) {
        num = bitset(num, comp.component.id);
    }

    return num;
}

export const getComponentsHash = (components: ComponentConstructor[]): ComponentsHash => {
    let num = 0;
    for (const comp of components) {
        num = bitset(num, comp.id);
    }

    return num;
}

export const componentHashMatch = (hash: ComponentsHash, other: ComponentsHash): boolean => {
    return (other & hash) === other;
}