import { EntityManager } from './entity-manager';
import { EntityView } from './entity-view-factory';

export interface System {
    onEntityAdded?: (entity: any) => void;
    onEntityRemoved?: (entity: any) => void;

    update(dt: number): void;
    attachEntityManager(entityManager: EntityManager): void;
}

export class ComponentSystem implements System {
    private entityManager!: EntityManager;

    // eslint-disable-next-line
    update(delta: number): void {
        // empty
    }

    attachEntityManager(entityManager: EntityManager): void {
        this.entityManager = entityManager;
    }

    protected getEntityManager(): EntityManager {
        return this.entityManager;
    }
}

export type ValuesOf<T extends any[]> = T[number];

type ArrayElement<ArrayType> = ArrayType extends (infer ElementType)[] ? ElementType : never;

export type SystemEntityType<T extends System, View extends keyof T> =
    (T[View] extends EntityView ? ArrayElement<T[View]['entities']> : never) | any;