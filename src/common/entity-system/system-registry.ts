import { System } from "./system";
import { EntityView } from "./entity-view-factory";
import { ComponentFamilyRegistry } from "./component-family-registry";

const isEntityView = (obj: any): obj is EntityView => {
    return (obj as EntityView)._isEntityView;
}

export class SystemRegistry {
    public systems: System[] = [];

    public register(system: System, families: ComponentFamilyRegistry): void {
        for (const key in system) {
            if (isEntityView((system as any)[key])) {
                const view: EntityView = (system as any)[key];
                const family = families.get(view.components);

                view.entities = family.entities;
                if (view.onEntityAdded) {
                    family.onEntityAdded.push(view.onEntityAdded);
                }

                if (view.onEntityRemoved) {
                    family.onEntityRemoved.push(view.onEntityRemoved);
                }
            }
        }

        this.systems.push(system);
    }
}