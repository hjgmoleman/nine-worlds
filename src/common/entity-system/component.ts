let generatedComponentsIds = 0;

export abstract class Component {
    static readonly id: number;
    abstract reset(object: this, ...args: any[]): void;
}

export function component(constructor: any): any {
    constructor.id = generatedComponentsIds++;
}

type NonFunctionPropertyNames<T> = { [K in keyof T]: T[K] extends Function ? never : K }[keyof T];

type NonFunctionProperties<T> = Pick<T, NonFunctionPropertyNames<T>>;

export type EntityOf<T extends any> = NonFunctionProperties<T>;

export type ComponentConstructor = {
    new(): Component;
    id: number;
}

export interface ComponentInitialisator<T extends any = any> {
    component: T;
    args?: T['prototype']['reset'];
}