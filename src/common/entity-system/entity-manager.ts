import { Component, ComponentConstructor, ComponentInitialisator } from "./Component";
import { Entity } from './entity';
import { Archetype } from './archetype';
import { SystemRegistry } from './system-registry';
import { ComponentFamilyRegistry } from './component-family-registry';
import { System } from './system';

export class EntityManager {

    private systemRegistry: SystemRegistry;
    private familyRegistry: ComponentFamilyRegistry;
    private components: Component[] = [];
    private currentId = 0;

    constructor() {
        this.systemRegistry = new SystemRegistry();
        this.familyRegistry = new ComponentFamilyRegistry();
    }

    addComponentsToEntity(entity: Entity, components: ComponentInitialisator[]): void {
        entity.components.push(...components);
        for (const component of components) {
            this.getComponentInstance(component.component).reset(entity as any, ...component.args);
        }
        
        this.familyRegistry.addEntity(entity, components);
    }
    
    removeEntity(entity: Entity): void {
        this.familyRegistry.removeEntity(entity);
    }

    removeComponentsFromEntity(entity: Entity, components: ComponentConstructor[] | ComponentConstructor): void {
        if (!(components instanceof Array)) {
            this.familyRegistry.removeComponentFromEntity(entity, components);
        } else {
            components.forEach(comp => this.familyRegistry.removeComponentFromEntity(entity, comp))
        }
    }

    registerSystem<T extends System = System>(system: T): T {
        system.attachEntityManager(this)
        this.systemRegistry.register(system, this.familyRegistry);
        
        return system;
    }

    update(dt: number): void {
        for (const system of this.systemRegistry.systems) {
            system.update(dt);
        }
    }

    createEntity: createEntityFunc = ((components: ComponentInitialisator[]) => {
        const entity = new Entity([]);
        entity.id = this.currentId++;
        this.addComponentsToEntity(entity, components)
        
        return entity as any;
    }) as any;

    createArchetype(components: ComponentInitialisator[]): Archetype {
        return components;
    }

    private getComponentInstance(component: ComponentConstructor): Component {
        if (!this.components[component.id]) {
            this.components[component.id] = new component();
        }
        return this.components[component.id];
    }
}

type NonFunctionPropertyNames<T> = { [K in keyof T]: T[K] extends Function ? never : K }[keyof T];
type NonFunctionProperties<T> = Pick<T, NonFunctionPropertyNames<T>>;
type ComponentOf<T extends ComponentInitialisator> = NonFunctionProperties<T['component']['prototype']>;

type createEntityFunc = <T extends ComponentInitialisator,
    T2 extends ComponentInitialisator,
    T3 extends ComponentInitialisator,
    T4 extends ComponentInitialisator,
    T5 extends ComponentInitialisator,
    T6 extends ComponentInitialisator,
    T7 extends ComponentInitialisator,
    T8 extends ComponentInitialisator,
    T9 extends ComponentInitialisator,
    T10 extends ComponentInitialisator>(comps: ComponentsInitList<T, T2, T3, T4, T5, T6, T7, T8, T9, T10>)
    => ComponentOf<T> & ComponentOf<T2> & ComponentOf<T3> & ComponentOf<T4> & ComponentOf<T5> & ComponentOf<T6>
    & ComponentOf<T7> & ComponentOf<T8> & ComponentOf<T9> & ComponentOf<T10> & Entity;

type ComponentsInitList<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> =
    Partial<[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10]> | { length: any } & {
    '0'?: T1;
    '1'?: T2;
    '2'?: T3;
    '3'?: T4;
    '4'?: T5;
    '5'?: T6;
    '6'?: T7;
    '7'?: T8;
    '8'?: T9;
    '9'?: T10;
}