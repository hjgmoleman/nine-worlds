import { ComponentInitialisator } from "./component";

export class Entity {
    id = -1;
    components: ComponentInitialisator[];

    constructor(components: ComponentInitialisator[]) {
        this.components = components;
    }
}

