import { ComponentInitialisator } from './component';

export type Archetype = ComponentInitialisator[];