export const TYPES = {
    EntitySystem: Symbol.for("entity-system:entity-system"),
    ComponentSystemRegistry: Symbol.for("entity-system:component-system-registry"),
    EntityManager: Symbol.for("entity-system:entity-manager")
};