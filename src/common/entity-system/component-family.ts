import { componentHashHasComponent, componentHashMatch, getComponentsHash, ComponentsHash } from './component-family-hash';
import { ComponentConstructor } from './component';
import { Entity } from './entity';

export class ComponentFamily {

    readonly entities: Entity[];
    readonly hash: ComponentsHash;
    readonly components: ComponentConstructor[];
    
    onEntityAdded: ((entity: Entity) => void)[];
    onEntityRemoved: ((entity: Entity) => void)[];

    constructor(components: ComponentConstructor[]) {
        this.hash = getComponentsHash(components);
        this.entities = [];
        this.components = components;
        this.onEntityAdded = [];
        this.onEntityRemoved = [];
    }

    addEntity(entity: Entity): void {
        for (const callback of this.onEntityAdded) {
            callback(entity);
        }

        this.entities.push(entity);
    }

    match(components: ComponentConstructor[]): boolean {
        for (const comp of components) {
            if (!componentHashHasComponent(this.hash, comp)) {
                return false;
            }
        }

        return true;
    }

    matchHash(hash: ComponentsHash): boolean {
        return componentHashMatch(hash, this.hash);
    }


    has(component: ComponentConstructor): boolean {
        return componentHashHasComponent(this.hash, component);
    }

    removeEntity(entity: Entity): void {
        for (const callback of this.onEntityRemoved) {
            callback(entity);
        }

        const index = this.entities.indexOf(entity);
        if (index !== -1) {
            this.entities.splice(index, 1);
        }
    }
}

export type ComponentFamilyMap = { 
    [hash: number]: ComponentFamily 
};
