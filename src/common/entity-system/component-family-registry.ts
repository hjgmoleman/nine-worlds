import { ComponentFamilyMap, ComponentFamily } from './component-family';
import { ComponentsHash, getComponentsHash, getComponentsHashFromInitialisators } from './component-family-hash';
import { ComponentConstructor, ComponentInitialisator } from './component';
import { Entity } from './entity';

export class ComponentFamilyRegistry {
    componentsGroups: ComponentFamilyMap = {};

    get(components: ComponentConstructor[], hash?: ComponentsHash): ComponentFamily {
        hash = hash || getComponentsHash(components);

        if (!this.componentsGroups[hash]) {
            this.componentsGroups[hash] = new ComponentFamily(components);
        }

        return this.componentsGroups[hash];
    }

    addEntity(entity: Entity, components: ComponentInitialisator[]): void {
        const hash = getComponentsHashFromInitialisators(components);
        
        for (const groupHash in this.componentsGroups) {
            if (this.componentsGroups[groupHash].matchHash(hash)) {
                this.componentsGroups[groupHash].addEntity(entity);
            }
        }
    }

    removeComponentFromEntity(entity: Entity, component: ComponentConstructor): void {
        for (const groupHash in this.componentsGroups) {
            if (this.componentsGroups[groupHash].has(component)) {
                this.componentsGroups[groupHash].removeEntity(entity);
            }
        }
    }

    removeEntity(entity: Entity): void {
        const hash = getComponentsHashFromInitialisators(entity.components);

        for (const groupHash in this.componentsGroups) {
            if (this.componentsGroups[groupHash].matchHash(hash)) {
                this.componentsGroups[groupHash].removeEntity(entity);
            }
        }
    }
}