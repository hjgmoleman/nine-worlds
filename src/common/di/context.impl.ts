import { interfaces } from "inversify";
import { Context, ContextRegisterDelegate } from "./context";

export class ContextImpl implements Context {
    private container: interfaces.Container;

    constructor(container: interfaces.Container) {
        this.container = container;
    }
    
    get<T>(token: interfaces.ServiceIdentifier<T>): T {
        return this.container.get<T>(token);
    }

    put<T>(token: interfaces.ServiceIdentifier<T>, binding: ContextRegisterDelegate<T>): void {
        return binding(this.container.bind(token));
    }

    createChildContext() {
        return new ContextImpl(this.container.createChild());
    }
}