import { interfaces } from "inversify";

export const CONTEXT_TOKEN = Symbol.for("di.context");

export interface Context {
    put<T>(token: interfaces.ServiceIdentifier<T>, binding: ContextRegisterDelegate<T>): void;
    get<T>(token: interfaces.ServiceIdentifier<T>): T;
}

export interface ContextRegisterDelegate<T> {
    (binding: interfaces.BindingToSyntax<T>): void;
}