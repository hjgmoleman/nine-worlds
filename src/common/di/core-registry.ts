import { Context } from "./context";

export class CoreRegistry {

    private static context: Context;

    static getContext(): Context {
        return CoreRegistry.context;
    }

    static setContext(context: Context) {
        CoreRegistry.context = context;
    }
}