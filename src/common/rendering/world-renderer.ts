import { Camera } from "@babylonjs/core/Cameras/camera";

export interface WorldRenderer {
    getActiveCamera(): Camera;
    getLightCamera(): Camera;

    update(delta: number): void;
    render(): void;
    dispose(): void;
}