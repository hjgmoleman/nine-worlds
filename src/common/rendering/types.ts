export const TYPES = {
    TerrainMesher: Symbol.for("rendering:terrain-mesher"),
    ChunkMesher: Symbol.for("rednering:chunk-mesher"),
    RenderEngine: Symbol.for("rendering:render-engine"),
    WorldRenderer: Symbol.for("rendering:world-renderer"),
}

export default TYPES;