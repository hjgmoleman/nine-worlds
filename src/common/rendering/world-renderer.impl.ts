// import glvec3 from "gl-vec3";
// import { inject } from "inversify";

// import { Scene } from "@babylonjs/core/scene";
// import { FreeCamera } from "@babylonjs/core/Cameras/freeCamera";
// import { Octree } from "@babylonjs/core/Culling/Octrees/octree";
// import { OctreeBlock } from "@babylonjs/core/Culling/Octrees/octreeBlock";
// import { Engine } from "@babylonjs/core/Engines/engine";
// import { HemisphericLight } from "@babylonjs/core/Lights/hemisphericLight";
// import { StandardMaterial } from "@babylonjs/core/Materials/standardMaterial";
// import { Vector3, Color3, Color4 } from "@babylonjs/core/Maths/math";
// import { Mesh } from "@babylonjs/core/Meshes/mesh";
// import { OctreeSceneComponent } from "@babylonjs/core/Culling/Octrees/";
// import "@babylonjss/core/Meshes/meshBuilder";
// import { CanvasElementAccessor } from "./canvas-element-accessor";
// import { Material } from "@babylonjs/core/Materials/material";

// const defaults = {
//     showFPS: false,
//     antiAlias: true,
//     clearColor: [0.8, 0.9, 1],
//     ambientColor: [1, 1, 1],
//     lightDiffuse: [1, 1, 1],
//     lightSpecular: [1, 1, 1],
//     groundLightColor: [0.5, 0.5, 0.5],
//     useAO: true,
//     AOmultipliers: [0.93, 0.8, 0.5],
//     reverseAOmultiplier: 1.0,
//     preserveDrawingBuffer: true,
// }

// export class WorldRenderer {
//     useAO: boolean;
//     aoVals: number[];
//     revAoVal: number;
//     meshingCutoffTime: number;

//     private cameraLocBlock: number;
//     private resizeDebounce: number;
//     private pendingResize = false;
//     private highlightPositions: number[] = [];
//     private scene: Scene;
//     private engine: Engine;
//     private octree: Octree<any>;
//     private cameraHolder: Mesh;
//     private camera: FreeCamera;
//     private cameraScreen: Mesh;
//     private cameraScreenMaterial: Material;
//     private flatMaterial: Material;
//     private light: HemisphericLight;
    
//     @inject("rendering:canvas-accessor")
//     private getCanvas: CanvasElementAccessor;

//     constructor() {
//         this.useAO = !!defaults.useAO;
//         this.aoVals = defaults.AOmultipliers;
//         this.revAoVal = defaults.reverseAOmultiplier;
//         this.meshingCutoffTime = 6; // ms
//         this.resizeDebounce = 250; // ms
    
//         // set up babylon scene
//         this.initScene();
//     }

//     render(): void {
//         this.updateCameraForRender(this);
//         this.engine.beginFrame();
//         this.scene.render();
//         //fps_hook()
//         this.engine.endFrame();
//     }

//     initScene() {
//         this.engine = new Engine(this.getCanvas(), defaults.antiAlias, {
//             preserveDrawingBuffer: defaults.preserveDrawingBuffer
//         });
        
//         this.scene = new Scene(this.engine);
//         const scene = this.scene;

//         scene.detachControl();

//         // octree setup
//         scene._addComponent(new OctreeSceneComponent(scene));
//         this.octree = new Octree(_ => { });
//         this.octree.blocks = [];
        
//         scene._selectionOctree = this.octree;

//         // camera, and empty mesh to hold it, and one to accumulate rotations
//         this.cameraHolder = new Mesh("camera-holder", scene);
//         this.camera = new FreeCamera("camera", new Vector3(0, 0, 0), scene);
//         this.camera.parent = this.cameraHolder;
//         this.camera.minZ = .01;
//         this.cameraHolder.visibility = 0;

//         // plane obscuring the camera - for overlaying an effect on the whole view
//         this.cameraScreen = Mesh.CreatePlane('camera-screen', 10, scene);
//         this.addMeshToScene(this.cameraScreen);
//         this.cameraScreen.position.z = .1;
//         this.cameraScreen.parent = this.camera;
//         this.cameraScreenMaterial = this.makeStandardMaterial("camera-screen-material");
//         this.cameraScreen.material = this.cameraScreenMaterial;
//         this.cameraScreen.setEnabled(false);
//         this.cameraLocBlock = 0;

//         // apply some defaults
//         this.light = new HemisphericLight('light', new Vector3(0.1, 1, 0.3), scene);
    
//         scene.clearColor = convertToColor4(arrayToColor(defaults.clearColor));
//         scene.ambientColor = arrayToColor(defaults.ambientColor);
//         this.light.diffuse = arrayToColor(defaults.lightDiffuse);
//         this.light.specular = arrayToColor(defaults.lightSpecular);
//         this.light.groundColor = arrayToColor(defaults.groundLightColor);

//         // make a default flat material (used or clone by terrain, etc)
//         this.flatMaterial = this.makeStandardMaterial("flat-material");
//     }

//     getScene(): Scene {
//         return this.scene;
//     }

//     private makeStandardMaterial(name: string): Material {
//         var material = new StandardMaterial(name, this.scene);
//         material.specularColor.copyFromFloats(0, 0, 0);
//         material.ambientColor.copyFromFloats(1, 1, 1);
//         material.diffuseColor.copyFromFloats(1, 1, 1);
        
//         return material;
//     }

//     private onResize(): void {
//         if (!this.pendingResize) {
//             this.pendingResize = true
//             setTimeout(() => {
//                 this.engine.resize()
//                 this.pendingResize = false
//             }, this.resizeDebounce)
//         }
//     }

//     private highlightBlockFace(show: boolean, posArr, normArr) {
//         var m = this.getHighlightMesh(this);
        
//         if (show) {
//             // floored local coords for highlight mesh
//             this.noa.globalToLocal(posArr, null, this.highlightPositions);

//             // offset to avoid z-fighting, bigger when camera is far away
//             var dist = glvec3.dist(this.noa.camera._localGetPosition(), this.highlightPositions);
//             var slop = 0.001 + 0.001 * dist;
//             for (var i = 0; i < 3; i++) {
//                 if (normArr[i] === 0) {
//                     this.highlightPositions[i] += 0.5;
//                 } else {
//                     this.highlightPositions[i] += (normArr[i] > 0) ? 1 + slop : -slop;
//                 }
//             }

//             m.position.copyFromFloats(this.highlightPositions[0], this.highlightPositions[1], this.highlightPositions[2]);
//             m.rotation.x = (normArr[1]) ? Math.PI / 2 : 0;
//             m.rotation.y = (normArr[0]) ? Math.PI / 2 : 0;
//         }

//         m.setEnabled(show);
//     }

//     private addMeshToScene(mesh: any, isStatic: boolean, pos, containingChunk: any) {
//         // exit silently if mesh has already been added and not removed
//         if (mesh._noaContainingChunk) return
//         if (this._octree.dynamicContent.includes(mesh)) return
    
//         // find local position for mesh and move it there (unless it's parented)
//         if (!mesh.parent) {
//             if (!pos) pos = [mesh.position.x, mesh.position.y, mesh.position.z]
//             var lpos = []
//             this.noa.globalToLocal(pos, null, lpos)
//             mesh.position.copyFromFloats(lpos[0], lpos[1], lpos[2])
//         }
    
//         // statically tie to a chunk's octree, or treat as dynamic?
//         var addToOctree = false
//         if (isStatic) {
//             var chunk = _containingChunk ||
//                 this.noa.world._getChunkByCoords(pos[0], pos[1], pos[2])
//             addToOctree = !!(chunk && chunk.octreeBlock)
//         }
    
//         if (addToOctree) {
//             chunk.octreeBlock.entries.push(mesh)
//             mesh._noaContainingChunk = chunk
//         } else {
//             this._octree.dynamicContent.push(mesh)
//         }
    
//         if (isStatic) {
//             mesh.freezeWorldMatrix()
//             mesh.freezeNormals()
//         }
    
//         // add dispose event to undo everything done here
//         var remover = this.removeMeshFromScene.bind(this, mesh)
//         mesh.onDisposeObservable.add(remover)
//     }

//     private removeMeshFromScene(mesh) {
//         if (mesh._noaContainingChunk && mesh._noaContainingChunk.octreeBlock) {
//             removeUnorderedListItem(mesh._noaContainingChunk.octreeBlock.entries, mesh)
//         }
//         mesh._noaContainingChunk = null
//         removeUnorderedListItem(this.octree.dynamicContent, mesh)
//     }

//     private prepareChunkForRendering(chunk) {
//         var cs = chunk.size
//         var loc = []
//         this.noa.globalToLocal([chunk.x, chunk.y, chunk.z], null, loc)
//         var min = new Vector3(loc[0], loc[1], loc[2])
//         var max = new Vector3(loc[0] + cs, loc[1] + cs, loc[2] + cs)
//         chunk.octreeBlock = new OctreeBlock(min, max, undefined, undefined, undefined, $ => { })
//         this._octree.blocks.push(chunk.octreeBlock)
//     }

//     private disposeChunkForRendering(chunk) {
//         if (!chunk.octreeBlock) return
//         removeUnorderedListItem(this.octree.blocks, chunk.octreeBlock)
//         chunk.octreeBlock.entries.length = 0
//         chunk.octreeBlock = null
//     }

//     private rebaseOrigin(delta) {
//         var dvec = new Vector3(delta[0], delta[1], delta[2])
    
//         this.scene.meshes.forEach(mesh => {
//             // parented meshes don't live in the world coord system
//             if (mesh.parent) return
    
//             // move each mesh by delta (even though most are managed by components)
//             mesh.position.subtractInPlace(dvec)
    
//             if (mesh._isWorldMatrixFrozen) mesh.markAsDirty()
//         })
    
//         // update octree block extents
//         this._octree.blocks.forEach(octreeBlock => {
//             octreeBlock.minPoint.subtractInPlace(dvec)
//             octreeBlock.maxPoint.subtractInPlace(dvec)
//             octreeBlock._boundingVectors.forEach(v => {
//                 v.subtractInPlace(dvec)
//             })
//         })
//     }
// }

// const convertToColor4 = (color: Color3) => {
//     return new Color4(color.r, color.g, color.b, 1);
// }

// const arrayToColor = (a: number[]) => {
//     return new Color3(a[0], a[1], a[2]) ;
// }

// function updateCameraForRender(self) {
//     var cam = self.noa.camera
//     var tgtLoc = cam._localGetTargetPosition()
//     self._cameraHolder.position.copyFromFloats(tgtLoc[0], tgtLoc[1], tgtLoc[2])
//     self._cameraHolder.rotation.x = cam.pitch
//     self._cameraHolder.rotation.y = cam.heading
//     self._camera.position.z = -cam.currentZoom

//     // applies screen effect when camera is inside a transparent voxel
//     var cloc = cam._localGetPosition()
//     var off = self.noa.worldOriginOffset
//     var cx = Math.floor(cloc[0] + off[0])
//     var cy = Math.floor(cloc[1] + off[1])
//     var cz = Math.floor(cloc[2] + off[2])
//     var id = self.noa.getBlock(cx, cy, cz)
//     checkCameraEffect(self, id)
// }



// //  If camera's current location block id has alpha color (e.g. water), apply/remove an effect

// const checkCameraEffect = (self, id) => {
//     if (id === self._camLocBlock) return
//     if (id === 0) {
//         self._camScreen.setEnabled(false)
//     } else {
//         var matId = self.noa.registry.getBlockFaceMaterial(id, 0)
//         if (matId) {
//             var matData = self.noa.registry.getMaterialData(matId)
//             var col = matData.color
//             var alpha = matData.alpha
//             if (col && alpha && alpha < 1) {
//                 self._camScreenMat.diffuseColor.set(0, 0, 0)
//                 self._camScreenMat.ambientColor.set(col[0], col[1], col[2])
//                 self._camScreenMat.alpha = alpha
//                 self._camScreen.setEnabled(true)
//             }
//         }
//     }
//     self._camLocBlock = id
// }


// // make or get a mesh for highlighting active voxel
// const getHighlightMesh = (rendering) => {
//     var mesh = rendering._highlightMesh
//     if (!mesh) {
//         mesh = Mesh.CreatePlane("highlight", 1.0, rendering._scene)
//         var hlm = rendering.makeStandardMaterial('highlightMat')
//         hlm.backFaceCulling = false
//         hlm.emissiveColor = new Color3(1, 1, 1)
//         hlm.alpha = 0.2
//         mesh.material = hlm

//         // outline
//         var s = 0.5
//         var lines = Mesh.CreateLines("hightlightLines", [
//             new Vector3(s, s, 0),
//             new Vector3(s, -s, 0),
//             new Vector3(-s, -s, 0),
//             new Vector3(-s, s, 0),
//             new Vector3(s, s, 0)
//         ], rendering._scene)
//         lines.color = new Color3(1, 1, 1)
//         lines.parent = mesh

//         rendering.addMeshToScene(mesh)
//         rendering.addMeshToScene(lines)
//         rendering._highlightMesh = mesh
//     }
//     return mesh
// }
