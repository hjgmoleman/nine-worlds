import { LinkedList } from "../linked-list/linked-list";
import { StringFormatter } from "common/utilities/string";

export class Queue<T extends any> {
    private list: LinkedList<T>;

    constructor() {
        this.list = new LinkedList<T>();
    }

  
    isEmpty(): boolean {
        return !this.list.getHead();
    }

    peek(): T {
        if (!this.list.getHead()) {
            return null;
        }

        return this.list.getHead().value;
    }


    enqueue(value: T): void {
        this.list.append(value);
    }


    dequeue(): T {
        const removedHead = this.list.deleteHead();
        return removedHead ? removedHead.value : null;
    }

    toString(callback: StringFormatter<T>): string {
        return this.list.toString(callback);
    }
}