import { Quaternion } from "@babylonjs/core";
import { Yaw } from "./yaw";
import { Pitch } from "./pitch";
import { Roll } from "./roll";

import { Side } from "../../../world/block/block";

const indexFor = (yaw: Yaw, pitch: Pitch, roll: Roll): number => {
    return ((yaw.index << 4) + (pitch.index << 2) + roll.index);
}

export class Rotation {
    public static readonly ALL_ROTATIONS: Rotation[] = [];
    public static NORMALIZED_ROTATIONS: Map<number, Rotation> = new Map();
    public static HORIZONTAL_ROTATIONS: Rotation[] = [];
    public static REVERSE_ROTATIONS: Rotation[] = [];

    /**
     * The index used by .rotate(), among others. Ranges from 0 to 63 (4^3-1).
     */
    public readonly index: number;

    constructor(
        public readonly yaw: Yaw, 
        public readonly pitch: Pitch, 
        public readonly roll: Roll
    ) {
        this.index = indexFor(yaw, pitch, roll);
    }

    static none(): Rotation {
        return Rotation.rotateByAll(Yaw.NONE, Pitch.NONE, Roll.NONE);
    }

    static rotateByPitch(pitch: Pitch): Rotation {
        return Rotation.rotateByAll(Yaw.NONE, pitch, Roll.NONE);
    }

    static rotateByYaw(yaw: Yaw): Rotation {
        return Rotation.rotateByAll(yaw, Pitch.NONE, Roll.NONE);
    }

    static rotateByRoll(roll: Roll): Rotation {
        return Rotation.rotateByAll(Yaw.NONE, Pitch.NONE, roll);
    }

    static rotateByYawAndPitch(yaw: Yaw, pitch: Pitch): Rotation {
        return Rotation.rotateByAll(yaw, pitch, Roll.NONE);
    }

    static rotateByPitchAndRoll(pitch: Pitch, roll: Roll): Rotation {
        return Rotation.rotateByAll(Yaw.NONE, pitch, roll);
    }

    static rotateByYawAndRoll(yaw: Yaw, roll: Roll): Rotation {
        return Rotation.rotateByAll(yaw, Pitch.NONE, roll);
    }

    static rotateByAll(yaw: Yaw, pitch: Pitch, roll: Roll): Rotation {
        return Rotation.ALL_ROTATIONS[indexFor(yaw, pitch, roll)];
    }

    /**
     * Finds a reverse rotation to the specified one. Any side transformed by the rotation passed as a parameter, when
     * passed to the returned rotation will return the original Side.
     *
     * @param rotation Rotation to find reverse rotation to.
     *
     * @return Reverse rotation to the specified one.
     */
    static findReverse(rotation: Rotation): Rotation {
        return Rotation.REVERSE_ROTATIONS[rotation.index];
    }

    getQuat4(): Quaternion {
        return Quaternion.RotationYawPitchRoll(this.yaw.radians, this.pitch.radians, this.roll.radians).normalize();
    }

    rotateSide(side: Side): Side {
        let result: Side = side;
        result = result.rollClockwise(this.roll.increments);
        result = result.pitchClockwise(this.pitch.increments);
        result = result.yawClockwise(this.yaw.increments);
        
        return result;
    }
}

;(function(): void {
    function findReverseInternal(rotation: Rotation): Rotation {
        const frontResult: Side = rotation.rotateSide(Side.FRONT);
        const topResult: Side = rotation.rotateSide(Side.TOP);
    
        for (const possibility of Rotation.ALL_ROTATIONS) {
            if (possibility.rotateSide(frontResult) == Side.FRONT  && possibility.rotateSide(topResult) == Side.TOP) {
                return possibility;
            }
        }
    
        throw new Error("Unable to find reverse rotation");
    }
    
    function findDuplicateRotation(rotation: Rotation): number {
        const frontResult: Side = rotation.rotateSide(Side.FRONT);
        const topResult: Side = rotation.rotateSide(Side.TOP);
        let result = 127;
    
        Rotation.NORMALIZED_ROTATIONS.forEach((rot, key) => {
            if (rot.rotateSide(Side.FRONT) === frontResult && rot.rotateSide(Side.TOP) === topResult) {
                result = key;
                return false;
            }

            return true;
        });
           
        return result !== 127 ? result : null;
    }

    for (const pitch of Pitch.values) {
        for (const yaw of Yaw.values) {
            for (const roll of Roll.values) {

                const rotation = new Rotation(yaw, pitch, roll);
                Rotation.ALL_ROTATIONS.push(rotation);

                const duplicateIndex = findDuplicateRotation(rotation);
                if (duplicateIndex === null) {
                    Rotation.NORMALIZED_ROTATIONS.set(indexFor(yaw, pitch, roll), rotation);
                }
            }
        }
    }
    
    Rotation.HORIZONTAL_ROTATIONS = [
        Rotation.ALL_ROTATIONS[indexFor(Yaw.NONE, Pitch.NONE, Roll.NONE)],
        Rotation.ALL_ROTATIONS[indexFor(Yaw.CLOCKWISE_90, Pitch.NONE, Roll.NONE)],
        Rotation.ALL_ROTATIONS[indexFor(Yaw.CLOCKWISE_180, Pitch.NONE, Roll.NONE)],
        Rotation.ALL_ROTATIONS[indexFor(Yaw.CLOCKWISE_270, Pitch.NONE, Roll.NONE)]
    ]

    for (const rotation of Rotation.ALL_ROTATIONS) {
        Rotation.REVERSE_ROTATIONS[rotation.index] = findReverseInternal(rotation);
    }
})();