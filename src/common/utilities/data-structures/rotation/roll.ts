export class Roll {
    public static readonly NONE = new Roll(0b00, 0, 0);
    public static readonly CLOCKWISE_90 = new Roll(0b01, 0.5 * Math.PI, 1);
    public static readonly CLOCKWISE_180 = new Roll(0b10, Math.PI, 2);
    public static readonly CLOCKWISE_270 = new Roll(0b11, -0.5 * Math.PI, 3);

    public static readonly values = [Roll.NONE, Roll.CLOCKWISE_90, Roll.CLOCKWISE_180, Roll.CLOCKWISE_270];

    constructor(
        public index: number, 
        public radians: number, 
        public increments: number
    ) {
    }
}