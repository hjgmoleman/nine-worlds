export class Pitch {
    public static readonly NONE = new Pitch(0b00, 0, 0);
    public static readonly CLOCKWISE_90 = new Pitch(0b01, 0.5 * Math.PI, 1);
    public static readonly CLOCKWISE_180 = new Pitch(0b10, Math.PI, 2);
    public static readonly CLOCKWISE_270 = new Pitch(0b11, -0.5 * Math.PI, 3);
    
    public static readonly values = [Pitch.NONE, Pitch.CLOCKWISE_90, Pitch.CLOCKWISE_180, Pitch.CLOCKWISE_270];
    
    constructor(
        public index: number, 
        public radians: number, 
        public increments: number
    ) {
    }
}