export class Yaw {
    public static readonly NONE = new Yaw(0b00, 0, 0);
    public static readonly CLOCKWISE_90 = new Yaw(0b01, 0.5 * Math.PI, 1);
    public static readonly CLOCKWISE_180 = new Yaw(0b10, Math.PI, 2);
    public static readonly CLOCKWISE_270 = new Yaw(0b11, -0.5 * Math.PI, 3);

    public static readonly values = [Yaw.NONE, Yaw.CLOCKWISE_90, Yaw.CLOCKWISE_180, Yaw.CLOCKWISE_270];

    constructor(
        public index: number, 
        public radians: number, 
        public increments: number
    ) {
    }
}