import { StringFormatter } from 'common/utilities/string';

export class LinkedListNode<T> {
    
    public readonly value: T;
    public next: LinkedListNode<T>;
    
    constructor(value: T, next: LinkedListNode<T> = null) {
        this.value = value;
        this.next = next;
    }
  
    toString(callback?: StringFormatter<T>): string {
        return callback ? callback(this.value) : `${this.value}`;
    }
}