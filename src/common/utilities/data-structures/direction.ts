import { Vector3 } from "@babylonjs/core/Maths/math";

export class Direction {

    public static readonly UP = new Direction(Vector3.Up());
    public static readonly RIGHT = new Direction(Vector3.Right());
    public static readonly LEFT = new Direction(Vector3.Left());
    public static readonly BACKWARD = new Direction(Vector3.Backward());
    public static readonly FORWARD = new Direction(Vector3.Forward());
    public static readonly DOWN = new Direction(Vector3.Down());

    public static readonly reverseMap = new Map<Direction, Direction>([
        [Direction.UP, Direction.DOWN],
        [Direction.LEFT, Direction.RIGHT],
        [Direction.RIGHT, Direction.LEFT],
        [Direction.FORWARD, Direction.BACKWARD],
        [Direction.BACKWARD, Direction.FORWARD],
        [Direction.DOWN, Direction.UP]
    ]);
        
    constructor(public vector3: Vector3) {
    }

    /**
     * Determines which direction the player is facing
     *
     * @param x right/left
     * @param y top/bottom
     * @param z back/front
     * @return Side enum with the appropriate direction
     */
    static inDirection(x: number, y: number, z: number): Direction {
        if (Math.abs(x) > Math.abs(y)) {
            if (Math.abs(x) > Math.abs(z)) {
                return (x > 0) ? Direction.LEFT : Direction.RIGHT;
            }
        } else if (Math.abs(y) > Math.abs(z)) {
            return (y > 0) ? Direction.UP : Direction.DOWN;
        }
        return (z > 0) ? Direction.FORWARD : Direction.BACKWARD;
    }

    /**
     * Determines which horizontal direction the player is facing
     *
     * @param x right/left
     * @param z back/front
     * @return Side enum with the appropriate direction
     */
    static inHorizontalDirection(x: number, z: number): Direction {
        if (Math.abs(x) > Math.abs(z)) {
            return (x > 0) ? Direction.LEFT : Direction.RIGHT;
        }

        return (z > 0) ? Direction.FORWARD : Direction.BACKWARD;
    }

    /**
     * @return The opposite side to this side.
     */
    reverse(): Direction {
        return Direction.reverseMap.get(this);
    }
}