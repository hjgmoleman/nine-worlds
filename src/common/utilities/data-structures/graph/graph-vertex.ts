import { LinkedList } from 'common/utilities/data-structures/linked-list/linked-list';
import { LinkedListNode } from 'common/utilities/data-structures/linked-list/linked-list-node';
import { StringFormatter } from 'common/utilities/string';
import { GraphEdge } from './graph-edge';

export class GraphVertex {
    
    private value: any;
    private key: string;
    private edges: LinkedList<GraphEdge>;
    
    constructor(value: any, key?: string) {
        if (value === undefined) {
            throw new Error('Graph vertex must have a value');
        }

        const edgeComparator = (edgeA: GraphEdge, edgeB: GraphEdge): number => {
            if (edgeA.getKey() === edgeB.getKey()) {
                return 0;
            }

            return edgeA.getKey() < edgeB.getKey() ? -1 : 1;
        };

        // Normally you would store string value like vertex name.
        // But generally it may be any object as well
        this.value = value;
        this.key = key;
        this.edges = new LinkedList<GraphEdge>(edgeComparator);
    }

    addEdge(edge: GraphEdge): GraphVertex {
        this.edges.append(edge);

        return this;
    }

    deleteEdge(edge: GraphEdge): void {
        this.edges.delete(edge);
    }

    getNeighbors(): GraphVertex[] {
        const edges = this.edges.toArray();
                
        const neighborsConverter = (node: LinkedListNode<GraphEdge>): GraphVertex => {
            return node.value.getStartVertex() === this 
                ? node.value.getEndVertex() 
                : node.value.getStartVertex();
        };

        // Return either start or end vertex.
        // For undirected graphs it is possible that current vertex will be the end one.
        return edges.map(neighborsConverter);
    }

    getEdges(): GraphEdge[] {
        return this.edges.toArray().map(linkedListNode => linkedListNode.value);
    }

    getDegree(): number {
        return this.edges.toArray().length;
    }

    hasEdge(requiredEdge: GraphEdge): boolean {
        const edgeNode = this.edges.find({
            callback: edge => edge === requiredEdge,
        });

        return !!edgeNode;
    }

    hasNeighbor(vertex: GraphVertex): boolean {
        const vertexNode = this.edges.find({
            callback: edge => edge.getStartVertex() === vertex || edge.getEndVertex() === vertex,
        });

        return !!vertexNode;
    }

    findEdge(vertex: GraphVertex): GraphEdge | null {
        const edgeFinder = (edge: GraphEdge): boolean => {
            return edge.getStartVertex() === vertex || edge.getEndVertex() === vertex;
        };

        const edge = this.edges.find({ callback: edgeFinder });

        return edge ? edge.value : null;
    }

    getKey(): string {
        return this.key ? this.key : this.value.toString();
    }

    getValue(): any {
        return this.value;
    }

    deleteAllEdges(): GraphVertex {
        this.getEdges().forEach((edge: GraphEdge) => this.deleteEdge(edge));

        return this;
    }

    toString(callback: StringFormatter<any>): string {
        return callback ? callback(this.value) : `${this.value}`;
    }
}