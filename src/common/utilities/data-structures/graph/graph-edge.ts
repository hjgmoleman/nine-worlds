import { GraphVertex } from './graph-vertex';

export class GraphEdge {
    
    private startVertex: GraphVertex;
    private endVertex: GraphVertex;
    private weight: number;
    
    constructor(startVertex: GraphVertex, endVertex: GraphVertex, weight = 0) {
        this.startVertex = startVertex;
        this.endVertex = endVertex;
        this.weight = weight;
    }

    getStartVertex(): GraphVertex {
        return this.startVertex;
    }

    getEndVertex(): GraphVertex {
        return this.endVertex;
    }

    getWeight(): number {
        return this.weight;
    }
  
    getKey(): string {
        const startVertexKey = this.startVertex.getKey();
        const endVertexKey = this.endVertex.getKey();
  
        return `${startVertexKey}_${endVertexKey}`;
    }
  
    reverse(): GraphEdge {
        const tmp = this.startVertex;
        this.startVertex = this.endVertex;
        this.endVertex = tmp;
  
        return this;
    }
  
    toString(): string {
        return this.getKey();
    }
}