import { GraphVertex } from './graph-vertex';
import { GraphEdge } from './graph-edge';

export class Graph {
    
    private vertices: { [key: string]: GraphVertex };
    private edges: { [key: string]: GraphEdge };
    
    readonly isDirected: boolean;

    constructor(isDirected = false) {
        this.vertices = {};
        this.edges = {};
        this.isDirected = isDirected;
    }
  
    addVertex(newVertex: GraphVertex): Graph {
        this.vertices[newVertex.getKey()] = newVertex;
  
        return this;
    }
  
    getVertexByKey(vertexKey: string): GraphVertex {
        return this.vertices[vertexKey];
    }
  
    getNeighbors(vertex: GraphVertex): GraphVertex[] {
        return vertex.getNeighbors();
    }
  
    getAllVertices(): GraphVertex[] {
        return Object.values(this.vertices);
    }
  
    getAllEdges(): GraphEdge[] {
        return Object.values(this.edges);
    }
  
    addEdge(edge: GraphEdge): Graph {
        // Try to find and end start vertices.
        let startVertex = this.getVertexByKey(edge.getStartVertex().getKey());
        let endVertex = this.getVertexByKey(edge.getEndVertex().getKey());
  
        // Insert start vertex if it wasn't inserted.
        if (!startVertex) {
            this.addVertex(edge.getStartVertex());
            startVertex = this.getVertexByKey(edge.getStartVertex().getKey());
        }
  
        // Insert end vertex if it wasn't inserted.
        if (!endVertex) {
            this.addVertex(edge.getEndVertex());
            endVertex = this.getVertexByKey(edge.getEndVertex().getKey());
        }
  
        // Check if edge has been already added.
        if (this.edges[edge.getKey()]) {
            throw new Error('Edge has already been added before.');
        } else {
            this.edges[edge.getKey()] = edge;
        }
  
        // Add edge to the vertices.
        if (this.isDirected) {
            // If graph IS directed then add the edge only to start vertex.
            startVertex.addEdge(edge);
        } else {
            // If graph ISN'T directed then add the edge to both vertices.
            startVertex.addEdge(edge);
            endVertex.addEdge(edge);
        }
  
        return this;
    }
  
    deleteEdge(edge: GraphEdge): void {
        // Delete edge from the list of edges.
        if (this.edges[edge.getKey()]) {
            delete this.edges[edge.getKey()];
        } else {
            throw new Error('Edge not found in graph.');
        }
  
        // Try to find and end start vertices and delete edge from them.
        const startVertex = this.getVertexByKey(edge.getStartVertex().getKey());
        const endVertex = this.getVertexByKey(edge.getEndVertex().getKey());
  
        startVertex.deleteEdge(edge);
        endVertex.deleteEdge(edge);
    }
  
    findEdge(startVertex: GraphVertex, endVertex: GraphVertex): GraphEdge | null {
        const vertex = this.getVertexByKey(startVertex.getKey());
  
        if (!vertex) {
            return null;
        }
  
        return vertex.findEdge(endVertex);
    }
  
    findVertexByKey(vertexKey: string): GraphVertex {
        if (this.vertices[vertexKey]) {
            return this.vertices[vertexKey];
        }
  
        return null;
    }
  
    getWeight(): number {
        return this.getAllEdges().reduce((weight: number, graphEdge: GraphEdge) => {
            return weight + graphEdge.getWeight();
        }, 0);
    }
  
    reverse(): Graph {
        this.getAllEdges().forEach((edge: GraphEdge) => {
            // Delete straight edge from graph and from vertices.
            this.deleteEdge(edge);
  
            // Reverse the edge.
            edge.reverse();
  
            // Add reversed edge back to the graph and its vertices.
            this.addEdge(edge);
        });
  
        return this;
    }
  
    getVerticesIndices(): { [key: string]: number } {
        const verticesIndices: { [key: string]: number } = {};
        this.getAllVertices().forEach((vertex, index) => {
            verticesIndices[vertex.getKey()] = index;
        });
  
        return verticesIndices;
    }
  
    getAdjacencyMatrix(): number[][] {
        const vertices = this.getAllVertices();
        const verticesIndices = this.getVerticesIndices();
  
        // Init matrix with infinities meaning that there is no ways of
        // getting from one vertex to another yet.
        const adjacencyMatrix = Array(vertices.length).fill(null).map(() => {
            return Array(vertices.length).fill(Infinity);
        });
  
        // Fill the columns.
        vertices.forEach((vertex: GraphVertex, vertexIndex: number) => {
            vertex.getNeighbors().forEach((neighbor: GraphVertex) => {
                const neighborIndex = verticesIndices[neighbor.getKey()];
                adjacencyMatrix[vertexIndex][neighborIndex] = this.findEdge(vertex, neighbor).getWeight();
            });
        });
  
        return adjacencyMatrix;
    }
  
    toString(): string {
        return Object.keys(this.vertices).toString();
    }
}