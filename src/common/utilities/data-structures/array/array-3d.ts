export class Array3d<T> {
    private data: T[];
    public readonly sizeX: number;
    public readonly sizeY: number;
    public readonly sizeZ: number;
    protected sizeXZ: number;
    protected sizeXYZ: number;

    constructor(sizeX: number, sizeY: number, sizeZ: number) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
        
        this.sizeXZ = sizeX * sizeZ;
        this.sizeXYZ = this.sizeXZ * sizeY;

        this.data = this.createStorage(sizeX, sizeY, sizeZ);
    }

    pos(x: number, y: number, z: number): number {
        return y * this.sizeXZ + z * this.sizeX + x;
    }

    get(x: number, y: number, z: number): T {
        return this.data[this.pos(x, y, z)];
    }

    set(x: number, y: number, z: number, value: T): void {
        this.data[this.pos(x, y, z)] = value;
    }

    contains(x: number, y: number, z: number): boolean {
        return (x >= 0 && x < this.sizeX && y >= 0 && y < this.sizeY && z >= 0 && z < this.sizeZ);
    }

    protected createStorage(x: number, y: number, z: number): T[] {
        return new Array(x * y * z);
    }
}