import { LinkedList } from 'common/utilities/data-structures/linked-list/linked-list';
import { LinkedListNode } from 'common/utilities/data-structures/linked-list/linked-list-node';
import { StringFormatter } from 'common/utilities/string';

export class Stack<T extends any> {
    private readonly linkedList: LinkedList<T>;
    
    constructor() {
        this.linkedList = new LinkedList<T>()
    }

    isEmpty(): boolean {
        return !this.linkedList.getTail();
    }

    peek(): T {
        if (this.isEmpty()) {
            return null;
        }

        return this.linkedList.getTail().value;
    }

    push(value: T): void {
        this.linkedList.append(value);
    }

    pop(): T {
        const removedTail = this.linkedList.deleteTail();
        return removedTail ? removedTail.value : null;
    }

    toArray(): T[] {
        return this.linkedList
            .toArray()
            .map((linkedListNode: LinkedListNode<T>) => linkedListNode.value)
            .reverse();
    }

    toString(callback: StringFormatter<T>): string {
        return this.linkedList.toString(callback);
    }
}