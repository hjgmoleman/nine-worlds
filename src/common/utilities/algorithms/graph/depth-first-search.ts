import { Graph } from "../../data-structures/graph/graph";
import { GraphVertex } from "../../data-structures/graph/graph-vertex";


export interface DFSCallbackParam {
    currentVertex?: GraphVertex;
    previousVertex?: GraphVertex;
    nextVertex?: GraphVertex;
}

export interface DFSCallbacks {
    allowTraversal?: (vertices: DFSCallbackParam) => boolean;
    enterVertex?: (vertices: DFSCallbackParam) => void;
    leaveVertex?: (vertices: DFSCallbackParam) => void;
}

/**
 * @param {Callbacks} [callbacks]
 * @returns {Callbacks}
 */
function initCallbacks(callbacks: Partial<DFSCallbacks> = {}): DFSCallbacks {
    const initiatedCallback: DFSCallbacks = callbacks;
  
    const stubCallback: any = () => { 
        // empty
    };
  
    const allowTraversalCallback = ((): (vertices: DFSCallbackParam) => boolean => {
        const seen: { [ key: string]: boolean } = {};

        return ({ nextVertex }: DFSCallbackParam): boolean => {
            if (!seen[nextVertex.getKey()]) {
                seen[nextVertex.getKey()] = true;
                return true;
            }

            return false;
        };
    })();
  
    initiatedCallback.allowTraversal = callbacks.allowTraversal || allowTraversalCallback;
    initiatedCallback.enterVertex = callbacks.enterVertex || stubCallback;
    initiatedCallback.leaveVertex = callbacks.leaveVertex || stubCallback;
  
    return initiatedCallback;
}
  
  /**
   * @param {Graph} graph
   * @param {GraphVertex} currentVertex
   * @param {GraphVertex} previousVertex
   * @param {Callbacks} callbacks
   */
function depthFirstSearchRecursive(graph: Graph, currentVertex: GraphVertex, previousVertex: GraphVertex, callbacks: DFSCallbacks): void {
    callbacks.enterVertex({ currentVertex, previousVertex });
  
    graph.getNeighbors(currentVertex).forEach((nextVertex) => {
        if (callbacks.allowTraversal({ previousVertex, currentVertex, nextVertex })) {
            depthFirstSearchRecursive(graph, nextVertex, currentVertex, callbacks);
        }
    });
  
    callbacks.leaveVertex({ currentVertex, previousVertex });
}
  
  /**
   * @param {Graph} graph
   * @param {GraphVertex} startVertex
   * @param {Callbacks} [callbacks]
   */
export function depthFirstSearch(graph: Graph, startVertex: GraphVertex, callbacks: DFSCallbacks): void {
    const previousVertex: GraphVertex = null;
    depthFirstSearchRecursive(graph, startVertex, previousVertex, initCallbacks(callbacks));
}