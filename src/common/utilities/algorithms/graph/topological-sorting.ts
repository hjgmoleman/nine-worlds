import { Stack } from "common/utilities/data-structures/stack/stack";
import { Graph } from "common/utilities/data-structures/graph/graph";
import { GraphVertex } from "common/utilities/data-structures/graph/graph-vertex";
import { depthFirstSearch, DFSCallbackParam } from "./depth-first-search";

export function topologicalSort(graph: Graph): GraphVertex[] {
    // Create a set of all vertices we want to visit.
    const unvisitedSet: { [key: string]: GraphVertex } = {};
    graph.getAllVertices().forEach((vertex) => {
        unvisitedSet[vertex.getKey()] = vertex;
    });

    // Create a set for all vertices that we've already visited.
    const visitedSet: { [key: string]: GraphVertex } = {};

    // Create a stack of already ordered vertices.
    const sortedStack = new Stack<GraphVertex>();

    const dfsCallbacks = {
        enterVertex: ({ currentVertex }: DFSCallbackParam): void => {
            // Add vertex to visited set in case if all its children has been explored.
            visitedSet[currentVertex.getKey()] = currentVertex;

            // Remove this vertex from unvisited set.
            delete unvisitedSet[currentVertex.getKey()];
        },
        leaveVertex: ({ currentVertex }: DFSCallbackParam): void => {
            // If the vertex has been totally explored then we may push it to stack.
            sortedStack.push(currentVertex);
        },
        allowTraversal: ({ nextVertex }: DFSCallbackParam): boolean => {
            return !visitedSet[nextVertex.getKey()];
        }
    };

    // Let's go and do DFS for all unvisited nodes.
    while (Object.keys(unvisitedSet).length) {
        const currentVertexKey = Object.keys(unvisitedSet)[0];
        const currentVertex = unvisitedSet[currentVertexKey];

        // Do DFS for current node.
        depthFirstSearch(graph, currentVertex, dfsCallbacks);
    }

    return sortedStack.toArray();
}