export interface StringFormatter<T extends any> {
    (arg: T | null): string;
}