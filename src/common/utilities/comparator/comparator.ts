export interface CompareFunction<T> {
    (a: T, b: T): number;
}

export class Comparator<T extends any> {
    private compare: CompareFunction<T>;
    
    constructor(compareFunction?: CompareFunction<T>) {
        this.compare = compareFunction || Comparator.defaultCompareFunction as any;
    }
  
    static defaultCompareFunction(a: string | number, b: string | number): number {
        if (a === b) {
            return 0;
        }

        return a < b ? -1 : 1;
    }
  
    equal(a: T, b: T): boolean {
        return this.compare(a, b) === 0;
    }
  
    lessThan(a: T, b: T): boolean {
        return this.compare(a, b) < 0;
    }
  
    greaterThan(a: T, b: T): boolean {
        return this.compare(a, b) > 0;
    }
  
    lessThanOrEqual(a: T, b: T): boolean {
        return this.lessThan(a, b) || this.equal(a, b);
    }
  
    greaterThanOrEqual(a: T, b: T): boolean {
        return this.greaterThan(a, b) || this.equal(a, b);
    }
  
    reverse(): void {
        const compareOriginal = this.compare;
        this.compare = (a: T, b: T): number => compareOriginal(b, a);
    }
}