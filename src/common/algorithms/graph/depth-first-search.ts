import { Graph } from 'common/utilities/data-structures/graph/graph';
import { GraphVertex } from 'common/utilities/data-structures/graph/graph-vertex';

/**
 * @typedef {Object} Callbacks
 *
 * @property {function(vertices: Object): boolean} [allowTraversal] -
 *  Determines whether DFS should traverse from the vertex to its neighbor
 *  (along the edge). By default prohibits visiting the same vertex again.
 *
 * @property {function(vertices: Object)} [enterVertex] - Called when DFS enters the vertex.
 * @property {function(vertices: Object)} [leaveVertex] - Called when DFS leaves the vertex.
 */

export interface Callbacks {
    allowTraversal?: (vertices: VertexTraversalParams) => boolean;
    enterVertex?: (vertices: EnterLeaveVertexParams) => void;
    leaveVertex?: (vertices: EnterLeaveVertexParams) => void;
}

export interface VertexTraversalParams {
    currentVertex: GraphVertex;
    previousVertex: GraphVertex;
    nextVertex: GraphVertex;
}

export interface EnterLeaveVertexParams {
    currentVertex: GraphVertex;
    previousVertex: GraphVertex;
}

const initCallbacks = (callbacks: Callbacks = {}): Callbacks => {
    const initiatedCallback = callbacks;
    const stubCallback = (): void => {
        // stub
    };

    const allowTraversalCallback = ((): any => {
        const seen: { [key: string]: boolean } = {};
        
        return ({ nextVertex }: VertexTraversalParams): boolean => {
            if (!seen[nextVertex.getKey()]) {
                seen[nextVertex.getKey()] = true;
                
                return true;
            }
            
            return false;
        };
    })();

    initiatedCallback.allowTraversal = callbacks.allowTraversal || allowTraversalCallback;
    initiatedCallback.enterVertex = callbacks.enterVertex || stubCallback;
    initiatedCallback.leaveVertex = callbacks.leaveVertex || stubCallback;

    return initiatedCallback;
}

const depthFirstSearchRecursive = (graph: Graph, currentVertex: GraphVertex, previousVertex: GraphVertex, callbacks: Callbacks): void => {
    callbacks.enterVertex({ currentVertex, previousVertex });

    graph.getNeighbors(currentVertex).forEach((nextVertex: GraphVertex) => {
        if (callbacks.allowTraversal({ previousVertex, currentVertex, nextVertex })) {
            depthFirstSearchRecursive(graph, nextVertex, currentVertex, callbacks);
        }
    });

    callbacks.leaveVertex({ currentVertex, previousVertex });
}

export const depthFirstSearch = (graph: Graph, startVertex: GraphVertex, callbacks?: Callbacks): void => {
    const previousVertex: GraphVertex = null;

    depthFirstSearchRecursive(graph, startVertex, previousVertex, initCallbacks(callbacks));
}