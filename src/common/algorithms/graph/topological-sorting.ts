import { Stack } from 'common/utilities/data-structures/stack/stack';
import { Graph } from 'common/utilities/data-structures/graph/graph';
import { GraphVertex } from 'common/utilities/data-structures/graph/graph-vertex';

import { 
    depthFirstSearch, 
    Callbacks,
} from './depth-first-search';

export const topologicalSort = (graph: Graph): any[] => {
    // Create a set of all vertices we want to visit.
    const unvisitedSet: { [key: string]: any } = {};

    graph.getAllVertices().forEach((vertex: GraphVertex): void => {
        unvisitedSet[vertex.getKey()] = vertex;
    });

    // Create a set for all vertices that we've already visited.
    const visitedSet: { [key: string]: any } = {};

    // Create a stack of already ordered vertices.
    const sortedStack = new Stack();

    const dfsCallbacks: Callbacks = {
        enterVertex: ({ currentVertex }): void => {
            // Add vertex to visited set in case if all its children has been explored.
            visitedSet[currentVertex.getKey()] = currentVertex;

            // Remove this vertex from unvisited set.
            delete unvisitedSet[currentVertex.getKey()];
        },
        
        leaveVertex: ({ currentVertex }): void => {
            // If the vertex has been totally explored then we may push it to stack.
            sortedStack.push(currentVertex);
        },

        allowTraversal: ({ nextVertex }) => {
            return !visitedSet[nextVertex.getKey()];
        }, 
    };

    // Let's go and do DFS for all unvisited nodes.
    while (Object.keys(unvisitedSet).length) {
        const currentVertexKey = Object.keys(unvisitedSet)[0];
        const currentVertex = unvisitedSet[currentVertexKey];

        // Do DFS for current node.
        depthFirstSearch(graph, currentVertex, dfsCallbacks);
    }

    return sortedStack.toArray();
}