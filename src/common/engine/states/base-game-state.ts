/* eslint-disable-file */
import { GameState } from "../game-state";
import { GameEngine } from "../game-engine";
import { Context } from "common/di/context";

export class BaseGameState implements GameState {
    
    getContext(): Context {
        return null;
    }

    init(engine: GameEngine): void {
        // empty
    }    
    
    update(delta: number): void {
        // empty
    }

    handleInput(delta: number): void {
        // empty
    }

    render(): void {
        // empty
    }

    dispose(): void {
        // empty
    }
}