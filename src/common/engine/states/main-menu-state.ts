import { World } from "ecsy";
import { CoreRegistry } from "common/di/core-registry";
import { TYPES as EntitySystemTypes } from "common/entity-system/types";
import { GameState } from "../game-state";
import { GameEngine } from "../game-engine";
import { CameraTargetSystem } from "common/camera/camera-target-system";
import { TYPES as CameraTypes } from "common/camera/types";
import { ClientComponent } from "common/network/client-component";
import { LocalPlayer } from "common/logic/local-player";
import { TYPES as LogicTypes } from "common/logic/types";
import { Context } from "common/di/context";

export class MainMenuGameState implements GameState {
    private context: Context;

    getContext(): Context {
        return this.context;
    }

    init(engine: GameEngine): void {
        this.context = engine.createChildContext();
        CoreRegistry.setContext(this.context); // not that pretty

        this.registerEntityRelatedServices(this.context);
    }    
    
    update(delta: number): void {
        // nothing
    }

    handleInput(delta: number): void {
        throw new Error("Method not implemented.");
    }

    render(): void {
        
    }

    dispose(): void {
        throw new Error("Method not implemented.");
    }

    private registerEntityRelatedServices(context: Context): void {
        // register objects
        const entityWorld = new World()
            .registerSystem(CameraTargetSystem);

        context.put<World>(EntitySystemTypes.EntitySystem, bind => bind.toConstantValue(entityWorld));

        // camera target system
        context.put<CameraTargetSystem>(CameraTypes.CameraTargetSystem, 
            bind => bind.toConstantValue(entityWorld.getSystem(CameraTargetSystem)));

        // create local player
        const playerEntity = entityWorld.createEntity()
            .addComponent(ClientComponent);

        const localPlayer = new LocalPlayer();
        localPlayer.setClientEntity(playerEntity);
        context.put<LocalPlayer>(LogicTypes.LocalPlayer, bind => bind.toConstantValue(localPlayer));
    }
}