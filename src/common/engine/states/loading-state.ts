import { CoreRegistry } from "common/di/core-registry";
import { Context } from "common/di/context";
import { BaseGameState } from "./base-game-state";
import { GameEngine } from "../game-engine";
import { Queue } from "common/utilities/data-structures/queue/queue";
import { InGameState } from "./ingame-state";

export class LoadingState extends BaseGameState {
    private context: Context;
    private loadingSteps: Queue<any> = new Queue();
    private engine: GameEngine;

    getContext(): Context {
        return this.context;
    }

    init(engine: GameEngine): void {
        this.engine = engine;
        this.context = engine.createChildContext();
        CoreRegistry.setContext(this.context);
    }

    update(): void {
        // empty
        if (this.loadingSteps.isEmpty()) {
            // change state to ingame
            this.engine.changeState(new InGameState(this.context));
        }
    }

    dispose(): void {
        // empty
    }
}
