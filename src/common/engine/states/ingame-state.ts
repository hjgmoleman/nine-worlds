//import { GameEngine } from 'nw/engine/game-engine';
import { BaseGameState } from "./base-game-state";
import { WorldRenderer } from "common/rendering/world-renderer";
import { TYPES as RenderingTypes } from "common/rendering/types";
import { Context } from "common/di/context";
import { TYPES as EntitySystemTypes } from "common/entity-system/types";
import { EntityManager } from "common/entity-system/entity-manager";

export class InGameState extends BaseGameState {
    
    private context: Context;
    private worldRenderer: WorldRenderer;
    private entityManager: EntityManager;
    private isPaused = false;

    constructor(context: Context) {
        super();

        this.context = context;
    }

    getContext(): Context {
        return this.context;
    }

    init(): void {
        this.worldRenderer = this.context.get(RenderingTypes.WorldRenderer);
        this.entityManager = this.context.get(EntitySystemTypes.EntityManager);
    }    

    update(delta: number): void {

        // update entity component systems
        this.entityManager.update(delta);

        // update the world renderer
        if (this.worldRenderer != null && this.shouldUpdateWorld()) {
            this.worldRenderer.update(delta);
        }
    }
    
    render(): void {
        if (this.worldRenderer != null) {
            this.worldRenderer.render();
        }

        this.renderUserInterface();
    }

    pause(): void {
        this.isPaused = true;
    }

    unpause(): void {
        this.isPaused = false;
    }

    private renderUserInterface(): void {
        // nothing yet
    }

    private shouldUpdateWorld(): boolean { 
        return !this.isPaused;
    }
}