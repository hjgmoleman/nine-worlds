export interface SignalListener {
    (...args: any[]): any;
}

export class Signal {
    private listeners: SignalListener[] = [];
    private lookup: Map<SignalListener, number>;

    constructor() {
        this.lookup = new Map();
    }

    bind(listener: SignalListener, thisRef: any): void {
        // param is an optional extra parameter for some internal functionality
        this.lookup.set(listener, this.listeners.length);

        const callback = thisRef ? listener.bind(thisRef) : listener;
        this.listeners.push(callback);
    }

    /**
     * Removes a function as a listener.
     */
    unbind(listener: SignalListener): void
    {
        const index = this.lookup.get(listener);
        if (index !== undefined) {
			this.listeners.splice(index, 1);
			this.lookup.delete(listener);
		}
    }

    /**
     * Unbinds all bound functions.
     */
    unbindAll(): void {
        this.listeners = [];
        this.lookup.clear();
    }

    /**
     * Dispatches the signal, causing all the listening functions to be called.
     *
     * @param [payload] An optional amount of arguments to be passed in as a parameter to the listening functions. Can be used to provide data.
     */
    dispatch(...args: any[]): void {
        const len = this.listeners.length;
        for (let i = 0; i < len; ++i)
            this.listeners[i].apply(null, args);
    }

    /**
     * Returns whether there are any functions bound to the Signal or not.
     */
    hasListeners(): boolean {
        return this.listeners.length > 0;
    }
}