export const TYPES = {
    Time: Symbol.for("engine:time"),
    GameEngine: Symbol.for("engine:game-engine"),
    ModuleManager: Symbol.for("engine:module-manager")
};