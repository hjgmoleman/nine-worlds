import{ GameEngine } from "common/engine/game-engine";
import { Subsystem } from "common/engine/subsystems/subsystem";
import { TYPES as RenderingTypes } from "common/rendering/types";
import { Context } from "common/di/context";
import { Engine } from "@babylonjs/core/Engines/engine";

export class RenderingSystem implements Subsystem {
    initialise(engine: GameEngine, context: Context): void {
        // get container div
        const containerDiv = this.createOrGetContainerDivElement();
        const canvasElement = this.createCanvasElement();
        
        containerDiv.appendChild(canvasElement);
        console.log('adskahsdkjahsdkjahs');

        // engine
        const renderEngine = new Engine(canvasElement, true, {
            preserveDrawingBuffer: true
        });

        // register Engine
        context.put<Engine>(RenderingTypes.RenderEngine, bind => bind.toConstantValue(renderEngine));
    }

    private createCanvasElement(): HTMLCanvasElement {
        const canvas = document.createElement("canvas");
        canvas.style.position = "absolute";
        canvas.style.left = "0px";
        canvas.style.top = "0px";
        canvas.style.height = "100%";
        canvas.style.width = "100%";
        canvas.id = "rendering-canvas";
        
        return canvas;
    }

    private createOrGetContainerDivElement(): HTMLDivElement {
        let container = document.getElementById("app") as HTMLDivElement;
        if (container == null) {
            container = this.createContainerDivElement();
        }

        container.tabIndex = 1;
        container.style.position = "fixed";
        container.style.left = "0px";
        container.style.right = "0px";
        container.style.top = "0px";
        container.style.bottom = "0px";
        container.style.height = "100%";
        container.style.overflow = "hidden";
        
        document.body.appendChild(container);

        return container;
    }

    private createContainerDivElement(): HTMLDivElement {
        const container = document.createElement("div");
        
        container.id = "app";
        
        return container;
    }
}
