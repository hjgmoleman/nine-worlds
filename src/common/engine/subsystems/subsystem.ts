import { GameEngine } from "../game-engine";
import { Context } from "common/di/context";

export interface Subsystem {
    initialise(engine: GameEngine, context: Context): void;   
}