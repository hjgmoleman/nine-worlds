import { Container, injectable } from "inversify";
import { Context } from "common/di/context";
import { ContextImpl } from "common/di/context.impl";
import { Subsystem } from "./subsystems/subsystem";
import { GameState } from "./game-state";
import { RenderingSystem } from "./subsystems/rendering-system";
import { TYPES as EngineTypes } from "./types";
import { Time } from "./time";
import { GameTime } from "./game-time";
import { CoreRegistry } from "common/di/core-registry";
import { ModuleManager } from "common/modules/module-manager";

@injectable()
export class GameEngine {
    
    protected subsystems: Subsystem[] = [];
    
    private additionalSubsystems: Subsystem[] = [];
    private context: ContextImpl;
    private currentState: GameState = null;
    private isStopRequested = false;
    private pendingNewState: GameState;
    private gameTime: GameTime;
    private moduleManager: ModuleManager = null;

    constructor() {
        this.context = new ContextImpl(new Container());
        this.context.put<GameEngine>(EngineTypes.GameEngine, bind => bind.toConstantValue(this));
        
        this.gameTime = new GameTime(this);
        this.context.put<Time>(EngineTypes.Time, bind => bind.toConstantValue(this.gameTime));

        this.moduleManager = new ModuleManager(this);
        this.context.put<ModuleManager>(EngineTypes.ModuleManager, bind => bind.toConstantValue(this.moduleManager));
    }

    isClient(): boolean {
        return true; // we only support clients, yet
    }

    stop(): void {
        this.isStopRequested = true;
    }

    run(state: GameState): void {
        this.initialise();
        this.currentState = state;
        this.gameTime.start();
    }

    registerAdditionalSubsystems(systems: Subsystem[]): void {
        this.additionalSubsystems = [ 
            ...this.additionalSubsystems, 
            ...systems
        ];
    }

    getGameState(): GameState {
        return this.currentState;
    }

    createChildContext(): Context {
        return this.context.createChildContext();
    }

    changeState(newState: GameState): void {
        if (this.currentState != null) {
            this.pendingNewState = newState;
        } else {
            this.switchState(newState);
        }
    }

    protected initialise(): void {
        this.initialiseManagers();
        this.initialiseSubsystems();
    }

    protected initialiseManagers(): void {
        // empty

    }

    protected initialiseSubsystems(): void {

        this.subsystems = [
            new RenderingSystem(),
            ...this.additionalSubsystems
            // new PhysicsSubsystem()
            // new CommandSubsystem()
            // new NetworkSubsystem()
            // new WorldGenerationSubsystem()
            // new GameSubsystem())
        ]

        this.subsystems.forEach(system => {
            system.initialise(this, this.context);
        });
    }

    private processPendingState(): void {
        if (this.pendingNewState != null) {
            this.switchState(this.pendingNewState);
            this.pendingNewState = null;
        }
    }

    private switchState(newState: GameState): void {
        if (this.currentState != null) {
            this.currentState.dispose();
        }

        this.currentState = newState;
        
        newState.init(this);
    }

    
    public update(delta: number, elapsedTime: number): void {
        this.processPendingState();
        CoreRegistry.setContext(this.currentState.getContext());
        this.currentState.update(delta, elapsedTime);
                
        if (this.isStopRequested) {
            this.gameTime.stop();
        }
    }

    public render(): void {
        this.currentState.render();
    }
}