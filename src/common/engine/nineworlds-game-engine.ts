import { GameEngine } from "common/engine/game-engine";
import { MainMenuGameState } from "./states/main-menu-state";

export class NineWorldsGameEngine extends GameEngine {
    start(): void {
        this.run(new MainMenuGameState());
    }
}