export interface Time {
    getFps(): number;
    getRealTimeInMs(): number;
    getGameDeltaInMs(): number;
    getGameDelta(): number;
}