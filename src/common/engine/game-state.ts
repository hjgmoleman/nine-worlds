import { GameEngine } from './game-engine';
import { Context } from "common/di/context";

export interface GameState {
    getContext(): Context;
    init(engine: GameEngine): void;
    update(delta: number, time?: number): void;
    handleInput(delta: number): void;
    render(): void;
    dispose(): void;
}