import { Entity } from "ecsy";
import { ClientComponent } from "common/network/client-component";

export class LocalPlayer {
    private clientEntity: Entity;

    constructor() {

    }

    setClientEntity(entity: Entity): void {
        this.clientEntity = entity;
        const clientComponent = entity.getMutableComponent(ClientComponent);
        if (clientComponent) {
            clientComponent.local = true;
        }
    }

    getClientEntity(): Entity {
        return this.clientEntity;
    }

    getCharacterEntity(): void {
        // empty
    }

    setCameraEntity(): void {
        // empty
    }

    getCameraEntity(): void {
        // empty
    }

    setClientInfoEntity(): void {
        // empty
    }

    getClientInfoEntity(): void {
        // empty
    }
}