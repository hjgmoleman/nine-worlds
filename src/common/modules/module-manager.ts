import { injectable } from "inversify";
import { Graph } from "common/utilities/data-structures/graph/graph";
import { GraphEdge } from "common/utilities/data-structures/graph/graph-edge";
import { GraphVertex } from "common/utilities/data-structures/graph/graph-vertex";
import { topologicalSort } from "common/utilities/algorithms/graph/topological-sorting"
import { ModuleLoader, Module, ModuleOptions, ModuleName } from "./module-factory";
import { GameEngine } from "common/engine/game-engine";
import { Signal } from "common/engine/signal";

@injectable()
export class ModuleManager {

    private require: NodeRequire;
    private loaders: { [moduleName: string]: ModuleLoader } = null;
    private modules: { [moduleName: string]: any } = null;
    private savedOptions: { [moduleName: string]: any } = null;
    private graph: Graph = null;
    private catchExceptions = true;
    private engine: GameEngine;

    public readonly moduleCreated: Signal = new Signal();
    public readonly moduleDestroyed: Signal = new Signal();
    public readonly moduleEnabled: Signal = new Signal();
    public readonly moduleDisabled: Signal = new Signal();

    constructor(engine: GameEngine) {
        this.engine = engine;
        this.loaders = {};
        this.require = require;
        
        // map plugin name to instances
        this.modules = {};
        
        this.savedOptions = {};
        this.graph = new Graph();
    }
    
    scan(name: ModuleName): ModuleLoader {
        let createModule: any; // factory for constructor
      
        if (name in this.loaders) {
            createModule = this.loaders[name];
        } else {
            createModule = this.require(name);
        }
      
        return createModule;
    }

    scanAndInstantiate(name: ModuleName, options: ModuleOptions = {}): boolean {
        if (this.get(name)) {
            console.log("Module already instantiated: ", name);
            return false;
        }
       
        const createModule = this.scan(name);
      
        if (createModule.moduleInfo && createModule.moduleInfo.clientOnly) {
            if (!this.engine.isClient) {
                console.log(`Skipping client-only module (${name}), in non-client environment.`);
                return false;
            }
        }
      
        if (!createModule) {
            console.log("Module not found: ", name);
            return false;
        }
      
        if (!this.wrapExceptions(() => {
            return this.instantiate(createModule, name, options) !== null;
        })) {
            console.log("failed to instantiate ", name);
        }
      
        return true;
    }

    instantiate(createModule: ModuleLoader, name: ModuleName, options: ModuleOptions): Module {
        const module = createModule(this.engine, options);
        if (!module) {
            console.log("create plugin failed:", name, createModule, module);
            return module; // null
        }
      
        module.moduleName = name;

        this.moduleCreated.dispatch(name);
      
        // plugins are enabled on instantiation -- assumed constructor calls its own enable() method (if present)
        module.enable();
        this.moduleEnabled.dispatch(name);
      
        this.modules[name] = module;
      
        return module;
    }

    preconfigure(name: ModuleName, options: ModuleOptions = {}): void {
        this.savedOptions[name] = options;
        
        if (!this.get(name)) {
            this.moduleCreated.dispatch(name);
        }
    }

    add(name: ModuleName, options: ModuleOptions = {}): boolean {
        if (options.onDemand) {
            this.preconfigure(name, options);
            
            return true;
        }
      
        const createModule = this.scan(name);
        if (!createModule){
            return false;
        }

        this.buildGraph(createModule, name);
      
        // save options to load with
        this.preconfigure(name, options);

        return true;
    }

    loadAll(): void {
        // topological sort by loadAfter dependency order
        const sortedModuleNames = topologicalSort(this.graph);
        for (let i = 0; i < sortedModuleNames.length; ++i) {
            const name = sortedModuleNames[i].getKey();
      
            if (!this.isEnabled(name)) {
                this.enableModule(name); // will instantiate() since preconfigured
            }
        }
    }

    get(name: ModuleName | Module): Module {
        if (typeof name === "string") {
            return this.modules[name];
        } else {
          // assume it is a plugin instance already, return as-is
          return name;
        }
    }

    isEnabled(name: ModuleName | Module): boolean {
        const module = this.get(name);
      
        return !!(module && module.isEnabled());
    }

    isLoaded(name: ModuleName | Module): boolean {
        const module = this.get(name);
      
        return !!(module && module.moduleName && this.modules[module.moduleName]);
    }

    listEnabledModules(): ModuleName[] {
        return this.listLoadedModules().filter(name => this.isEnabled(name));
    }

    listLoadedModules(): ModuleName[] {
        const loaded = Object.keys(this.modules);
        const unloaded = Object.keys(this.savedOptions).filter(x => loaded.indexOf(x) === -1);
      
        return loaded.concat(unloaded);
    }

    enableModule(name: ModuleName | Module): boolean {
        const module = this.get(name);
      
        if (!module) {
            const moduleName = name as ModuleName;
            if (this.savedOptions[moduleName]) {
                // on-demand instantiation, with prespecified options
                return this.scanAndInstantiate(moduleName, this.savedOptions[moduleName]);
            } else {
                console.log("no such module loaded to enable: ", module, moduleName);
            }
      
            return false;
        } else {
            if (module.isEnabled()) {
                console.log("already enabled: ", module, name);
                return false;
            }
      
            if (module.enable) {
                if (!this.wrapExceptions(() => {
                    module.enable();
                    return true;
                })) {
                    console.log("failed to enable:", module, name);
                    return false;
                }
            }
          
            this.moduleEnabled.dispatch(name);
        }
        return true;
    }

    disableModule(name: ModuleName | Module): boolean {
        console.log("disabling module ", name);
        const module = this.get(name); 
      
        if (!module) {
          console.log("no such module loaded to disable: ", module, name);
          return false;
        }

        if (!module.isEnabled()) {
            console.log("already disabled: ", module, name);
            return false;
        }
      
        if (module.disable) {
            if (!this.wrapExceptions(() => {
                module.disable();
                return true; 
            })) {
                console.log("failed to disable:", module, name);
                return false;
            }
        }
      
        this.moduleDisabled.dispatch(name);
      
        return true;
    }

    toggleModule(name: ModuleName | Module): boolean {
        if (this.isEnabled(name)) {
            return this.disableModule(name);
        } else {
            return this.enableModule(name);
        }
    }

    destroyModule(name: ModuleName | Module): boolean {
        const module = this.get(name);
      
        if (!module) {
            console.log("no module", module, name);
            return false;
        }
      
        if (!this.modules[module.moduleName]) {
            console.log("no such module to destroy: ", module);
            return false;
        }
      
        if (this.isEnabled(module))
            this.disableModule(module);
      
        delete this.modules[module.moduleName];
        console.log("destroyed  ", module);
      
        return true;
      }

    private wrapExceptions(f: () => boolean): boolean {
        let returnValue: boolean;
      
        if (!this.catchExceptions) {
            returnValue = f();
            return returnValue === undefined ? true : returnValue; // undefined ok
        }
      
        try {
            returnValue = f();
        } catch (e) {
            console.log('caught exception:',e,'calling',f);
            console.trace();
            return false;
        }

        return returnValue === undefined ? true : returnValue;
    }

    private buildGraph(createModule: ModuleLoader, name: ModuleName): void {
        let dependsOn: ModuleName[] = [];
      
        if (createModule.moduleInfo) {
            dependsOn = createModule.moduleInfo.dependsOn;
      
            if (!dependsOn) {
                dependsOn = [];
            }
        }
      
        this.graph.addVertex(new GraphVertex(name));

        // add edges for each plugin required to load before us
        for (let i = 0; i < dependsOn.length; ++i) {
            this.graph.addEdge(new GraphEdge(new GraphVertex(dependsOn[i]), new GraphVertex(name)));
        }
    }
}