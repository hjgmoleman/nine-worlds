import { GameEngine } from "common/engine/game-engine";

export type ModuleName = string;

export interface ModuleInfo {
    clientOnly: boolean;
    dependsOn?: ModuleName[];
}

export interface ModuleOptions {
    onDemand?: boolean;
}

export interface Module {
    moduleName: ModuleName;
    enable?: () => void;
    disable?: () => void;
    isEnabled(): boolean;
}

export interface ModuleLoader {
    <T extends ModuleOptions>(engine: GameEngine, options: T): Module;
    moduleInfo?: ModuleInfo;
}