import { Array3d } from "common/utilities/data-structures/array/array-3d";
import { Vector3 } from "@babylonjs/core/Maths/math";
import { Block } from "./block/block";
import { BlockManager } from "./block/block-manager";
import { Mesh } from "@babylonjs/core";
//import ndarray from "ndarray";
export const ChunkConstants = {
    SIZE_X: 32,
    SIZE_Y: 64,
    SIZE_Z: 32,
    SIZE: new Vector3(32, 64, 32)
}

export interface ChunkBlock {
    block: Block;
    position: Vector3;
}

export class Chunk {
   
    public chunkPosition: Vector3;
    public mesh: Mesh;
    public pendingMesh: Mesh;

    public isDirty: boolean;

    private blockManager: any;
    private blockData: Array3d<number>;
    private extraData: Array3d<number>[];

    constructor(position: Vector3, blockManager: BlockManager) {
        this.chunkPosition = position;
        this.blockManager = blockManager;
        this.isDirty = true;

        this.blockData = new Array3d(ChunkConstants.SIZE_X, ChunkConstants.SIZE_Y, ChunkConstants.SIZE_Z);
    }

    get chunkSizeX(): number {
        return ChunkConstants.SIZE_X;
    }

    get chunkSizeY(): number {
        return ChunkConstants.SIZE_Y;
    }

    get chunkSizeZ(): number {
        return ChunkConstants.SIZE_Z;
    }

    get chunkWorldOffset(): Vector3 {
        return new Vector3(this.chunkWorldOffsetX, this.chunkWorldOffsetY, this.chunkWorldOffsetZ);
    }

    get chunkWorldOffsetX(): number {
        return this.chunkPosition.x * this.chunkSizeX;
    }

    get chunkWorldOffsetY(): number {
        return this.chunkPosition.y * this.chunkSizeY;
    }

    get chunkWorldOffsetZ(): number {
        return this.chunkPosition.z * this.chunkSizeZ;
    }

    get hasMesh(): boolean {
        return this.mesh !== null;
    }

    [Symbol.iterator] = (): Iterator<ChunkBlock> => {
        const worldOffset = this.chunkWorldOffset;
        const data = this.blockData;
        const bm = this.blockManager;
        
        const endPos = new Vector3(data.sizeX, data.sizeY, data.sizeZ);
               
        let iterationCount = 0;

        for (let x = 0; x <= endPos.x ; x++) {
            for (let y = 0; y <= endPos.y ; y++) {
                for (let z = 0; z <= endPos.z ; z++) {
                    iterationCount++;
                    yield {
                        block: bm.getBlock(data.get(x + worldOffset.x, y + worldOffset.y, z + worldOffset.z)),
                        position: Vector3(x, y, z)
                    };
                }
            }    
        }

        return iterationCount;        
    }

    getBlock(x: number, y: number, z: number): Block {
        const id = this.blockData.get(x, y, z);
        
        return this.blockManager.getBlock(id);
    }

    setBlock(x: number, y: number, z: number, block: Block): Block {
        const oldBlockId = this.blockData.set(x, y, z, block.id);
        
        return this.blockManager.getBlock(oldBlockId);
    }

    getExtraData(index: number, x: number, y: number, z: number): number {
        return this.extraData[index].get(x, y, z);
    }

    setExtraData(index: number, x: number, y: number, z: number, value: number): void {
        this.extraData[index].set(x, y, z, value);
    }

    toString(): string {
        return "Chunk " + this.chunkPosition.toString();
    }
}
