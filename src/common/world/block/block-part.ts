import { Side } from "./side";
import { Rotation } from "../../utilities/data-structures/rotation/rotation";

export class BlockPart {
    public static readonly TOP = new BlockPart(Side.TOP);
    public static readonly LEFT = new BlockPart(Side.LEFT);
    public static readonly RIGHT = new BlockPart(Side.RIGHT);
    public static readonly FRONT = new BlockPart(Side.FRONT);
    public static readonly BACK = new BlockPart(Side.BACK);
    public static readonly BOTTOM = new BlockPart(Side.BOTTOM);
    public static readonly CENTER = new BlockPart();

    private static readonly sidemap = new Map<Side, BlockPart>([
        [Side.BOTTOM, BlockPart.BOTTOM],
        [Side.RIGHT, BlockPart.RIGHT],
        [Side.LEFT, BlockPart.LEFT],
        [Side.BACK, BlockPart.BACK],
        [Side.FRONT, BlockPart.FRONT],
        [Side.TOP, BlockPart.TOP]
    ])
    
    public static readonly SIDES: BlockPart[] = [BlockPart.TOP, BlockPart.LEFT, BlockPart.RIGHT, BlockPart.FRONT, BlockPart.BACK, BlockPart.BOTTOM];
    public static readonly HORIZONTAL_SIDES = [BlockPart.LEFT, BlockPart.RIGHT, BlockPart.FRONT, BlockPart.BACK];

    constructor(public side?: Side) {
    }
    
    static fromSide(side: Side): BlockPart {
        return BlockPart.sidemap.get(side);
    }

    public isSide(): boolean {
        return !!this.side;
    }

    public rotate(rotation: Rotation): BlockPart {
        if (this.isSide()) {
            return BlockPart.fromSide(rotation.rotateSide(this.side));
        }

        return this;
    }
}