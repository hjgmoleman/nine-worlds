import { Rotation } from "../../utilities/data-structures/rotation/rotation";
import { Side } from "./side";

export class Block {
    public id: number;
    public uri: string;
    public displayName = "Untitled block";
    public rotation: Rotation = Rotation.none();


    get direction(): Side {
        return this.rotation.rotateSide(Side.FRONT);
    }
}