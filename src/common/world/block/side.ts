import { Vector3 } from "@babylonjs/core/Maths/math";
import { Direction } from "../../utilities/data-structures/direction";

export class Side {

    public static readonly TOP = new Side(Vector3.Up(), true, false, true);
    public static readonly BOTTOM = new Side(Vector3.Down(), true, false, true);
    public static readonly LEFT = new Side(Vector3.Left(), false, true, true);
    public static readonly RIGHT = new Side(Vector3.Right(), false, true, true);
    public static readonly FRONT = new Side(Vector3.Backward(), true, true, false);
    public static readonly BACK = new Side(Vector3.Forward(), true, true, false);

    public static readonly ALL = [Side.TOP, Side.BOTTOM, Side.LEFT, Side.RIGHT, Side.FRONT, Side.BACK];

    public static readonly tangents = new Map<Side, Side[]>([
        [Side.TOP,      [Side.LEFT, Side.RIGHT, Side.FRONT, Side.BACK]],
        [Side.BOTTOM,   [Side.LEFT, Side.RIGHT, Side.FRONT, Side.BACK]],
        [Side.LEFT,     [Side.TOP, Side.BOTTOM, Side.FRONT, Side.BACK]],
        [Side.RIGHT,    [Side.TOP, Side.BOTTOM, Side.FRONT, Side.BACK]],
        [Side.FRONT,    [Side.TOP, Side.BOTTOM, Side.LEFT, Side.RIGHT]],
        [Side.BACK,     [Side.TOP, Side.BOTTOM, Side.LEFT, Side.RIGHT]]
    ]);
        
    public static readonly reverseMap = new Map<Side, Side>([
        [Side.TOP, Side.BOTTOM],
        [Side.LEFT, Side.RIGHT],
        [Side.RIGHT, Side.LEFT],
        [Side.FRONT, Side.BACK],
        [Side.BACK, Side.FRONT],
        [Side.BOTTOM, Side.TOP]
    ]);

    public static readonly conversionMap = new Map<Side, Direction>([
        [Side.TOP, Direction.UP],
        [Side.BOTTOM, Direction.DOWN],
        [Side.BACK, Direction.FORWARD],
        [Side.FRONT, Direction.BACKWARD],
        [Side.RIGHT, Direction.LEFT],
        [Side.LEFT, Direction.RIGHT]
    ]);

    public static readonly reversedConversionMap = new Map<Direction, Side>([
        [Direction.UP, Side.TOP],
        [Direction.DOWN, Side.BOTTOM],
        [Direction.FORWARD, Side.BACK],
        [Direction.BACKWARD, Side.FRONT],
        [Direction.LEFT, Side.RIGHT],
        [Direction.RIGHT, Side.LEFT]
    ]);
        
    private static readonly clockwiseYawSide = new Map<Side, Side>([
        [Side.FRONT, Side.LEFT],
        [Side.RIGHT, Side.FRONT],
        [Side.BACK, Side.RIGHT],
        [Side.LEFT, Side.BACK]
    ]);

    private static readonly anticlockwiseYawSide = new Map<Side, Side>([
        [Side.FRONT, Side.RIGHT],
        [Side.RIGHT, Side.BACK],
        [Side.BACK, Side.LEFT],
        [Side.LEFT, Side.FRONT]
    ]);

    private static readonly clockwisePitchSide = new Map<Side, Side>([
        [Side.FRONT, Side.TOP],
        [Side.BOTTOM, Side.FRONT],
        [Side.BACK, Side.BOTTOM],
        [Side.TOP, Side.BACK]
    ]);
        
    private static readonly anticlockwisePitchSide = new Map<Side, Side>([
        [Side.FRONT, Side.BOTTOM],
        [Side.BOTTOM, Side.BACK],
        [Side.BACK, Side.TOP],
        [Side.TOP, Side.FRONT]
    ]);
        
    private static readonly clockwiseRollSide = new Map<Side, Side>([
        [Side.TOP, Side.LEFT],
        [Side.LEFT, Side.BOTTOM],
        [Side.BOTTOM, Side.RIGHT],
        [Side.RIGHT, Side.TOP]
    ]);
    
    private static readonly anticlockwiseRollSide = new Map<Side, Side>([
        [Side.TOP, Side.RIGHT],
        [Side.LEFT, Side.TOP],
        [Side.BOTTOM, Side.LEFT],
        [Side.RIGHT, Side.BOTTOM]
    ]);

    public static readonly horizontalSides = [Side.LEFT, Side.RIGHT, Side.FRONT, Side.BACK];
    public static readonly verticalSides = [Side.TOP, Side.BOTTOM];

    constructor(
        public readonly vector3: Vector3, 
        public readonly canPitch: boolean, 
        public readonly canYaw: boolean, 
        public readonly canRoll: boolean
    ) {
    }

    isHorizontal(): boolean {
        return this.canYaw;
    }

    isVertical(): boolean {
        return !this.canYaw;
    }

    static inDirection(x: number, y: number, z: number): Side {
        if (Math.abs(x) > Math.abs(y)) {
            if (Math.abs(x) > Math.abs(z)) {
                return (x > 0) ? Side.RIGHT : Side.LEFT;
            }
        } else if (Math.abs(y) > Math.abs(z)) {
            return (y > 0) ? Side.TOP : Side.BOTTOM;
        }

        return (z > 0) ? Side.BACK : Side.FRONT;
    }
    
    static inHorizontalDirection(x: number, z: number): Side {
        if (Math.abs(x) > Math.abs(z)) {
            return (x > 0) ? Side.RIGHT : Side.LEFT;
        }

        return (z > 0) ? Side.BACK : Side.FRONT;
    }
    
    reverse(): Side {
        return Side.reverseMap.get(this);
    }

    yawClockwise(turns: number): Side {
        if (!this.canYaw) {
            return this;
        }

        let steps = turns;
        if (steps < 0) {
            steps = -steps + 2;
        }

        steps = steps % 4;

        switch (steps) {
            case 1:
            return Side.clockwiseYawSide.get(this);
        case 2:
            return Side.reverseMap.get(this);
        case 3:
            return Side.anticlockwiseYawSide.get(this);
        default:
            return this;
        }
    }
    
    pitchClockwise(turns: number): Side {
        if (!this.canPitch) {
            return this;
        }

        let steps = turns;
        if (steps < 0) {
            steps = -steps + 2;
        }

        steps = steps % 4;
        
        switch (steps) {
            case 1:
                return Side.clockwisePitchSide.get(this);
            case 2:
                return Side.reverseMap.get(this);
            case 3:
                return Side.anticlockwisePitchSide.get(this);
            default:
                return this;
        }
    }

    toDirection(): Direction {
        return Side.conversionMap.get(this);
    }

    fromDirection(direction: Direction): Side {
        return Side.reversedConversionMap.get(direction);
    }

    rollClockwise(turns: number): Side {
        if (!this.canRoll) {
            return this;
        }

        let steps = turns;
        if (steps < 0) {
            steps = -steps + 2;
        }

        steps = steps % 4;
        
        switch (steps) {
            case 1:
                return Side.clockwiseRollSide.get(this);
            case 2:
                return Side.reverseMap.get(this);
            case 3:
                return Side.anticlockwiseRollSide.get(this);
            default:
                return this;
        }
    }

     getAdjacentPos(position: Vector3): Vector3 {
        return position.add(this.vector3);
    }

    getRelativeSide(direction: Direction): Side {
        if (direction == Direction.UP) {
            return this.pitchClockwise(1);
        } else if (direction == Direction.DOWN) {
            return this.pitchClockwise(-1);
        } else if (direction == Direction.LEFT) {
            return this.yawClockwise(1);
        } else if (direction == Direction.RIGHT) {
            return this.yawClockwise(-1);
        } else if (direction == Direction.BACKWARD) {
            return this.reverse();
        } else {
            return this;
        }
    }

    tangents(): Side[] {
        return Side.tangents.get(this);
    }
}