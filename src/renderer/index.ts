import "reflect-metadata";
import { NineWorldsGameEngine } from "common/engine/nineworlds-game-engine";

// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
function main(): void {
    const game = new NineWorldsGameEngine();
    game.start();
}

main();

if ((module as any).hot) {
    (module as any).hot.accept();
}